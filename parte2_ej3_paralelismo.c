#include <stdio.h>
#include <stdlib.h>    
#include <unistd.h>     // para castear de puntero a entero
#include <string.h>
#include <time.h>
#include <pthread.h>

#define CANTIDAD_LUGARES 6
char* lugares_labels[] = {"Norte America", "Centro America", "Sud America", "Africa", "Europa", "Asia", "Oceania"};

void esperar () {
  //simular un tiempo de ejecucion de algun script araña (algun codigo en python)

  //inicializar en 1 segundo = 1000000 microseconds:
  int microseconds = 1000000;

  //dormir el thread, simula que esta haciendo alguna tarea
  usleep(microseconds);
}


void* recopilar_informacion (void* parametro)
{
int lugar=(intptr_t)parametro;
  printf("***************************************** \n");
  printf("Buscando informacion en  %s \n", lugares_labels[lugar]);
  esperar();
		pthread_exit(NULL);
}


int main ()
{	pthread_t  array_Threads[CANTIDAD_LUGARES];
	int thread ;

  int i;
  for (i = 0; i < CANTIDAD_LUGARES; i++){
	thread =pthread_create (&array_Threads[i],NULL,recopilar_informacion,(void*)(intptr_t) i);
    //recopilar_informacion(i);
  }
for (i=0;i<CANTIDAD_LUGARES; i++){
	pthread_join(array_Threads[i],NULL);
	}
pthread_exit(NULL);
}


//Para compilar:   gcc parte2_ej3_paralelismo.c -o ej3
//Para ejecutar:   time ./ej3

//tiempo de ejecucion de este programa:
//real	0m6.003s
//user	0m0.000s
//sys	0m0.002s



