#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

sem_t der;
sem_t horiz;
sem_t izq;
sem_t enter;
sem_t arriba;

int i;

    pthread_t T1;
    pthread_t T2;
    pthread_t T3;
    pthread_t T4;


void* func_Der(){
    int j=i;
  while(j>0){
        sem_wait(&der);
        sem_wait(&horiz);
            printf("→ ");
        sem_post(&izq);
        sem_post(&arriba);
    j--;
    }
        pthread_exit(NULL);
}


void* func_Izq(){
   int j=i;
   while(j>0){
        sem_wait(&izq);
        sem_wait(&horiz);
            printf("← ");
        sem_post(&arriba);
    j--;
    }
        pthread_exit(NULL);
}

void* func_Arriba(){
    int j=i*2;
    while(j>0){
        sem_wait(&arriba);
            printf("↑ ");
        sem_post(&enter);
        sem_post(&horiz);
    j--;
    }
    pthread_exit(NULL);
}

void* func_Enter(){
    int j=i;
    while(j>0){
        sem_wait(&enter);
        sem_wait(&enter);
            printf("ENTER\n");
            sem_post(&der);
    j--;
        }
    pthread_exit(NULL);
}

int main(){
             sem_init(&der,0,1);
             sem_init(&horiz,0,1);
             sem_init(&izq,0,0);
             sem_init(&enter,0,0);
             sem_init(&arriba,0,0);

        
            i=2;
            int thread;
                thread=pthread_create(&T1,NULL,func_Der,NULL);
                thread=pthread_create(&T2,NULL,func_Arriba,NULL);
                thread=pthread_create(&T3,NULL,func_Izq,NULL);
                thread=pthread_create(&T4,NULL,func_Enter,NULL);

             pthread_join(T1 , NULL);
             pthread_join(T2 , NULL);
             pthread_join(T3 , NULL);
             pthread_join(T4 , NULL);

if (thread){
                        printf("Error:unable to create thread, %d \n", thread);
                        exit(-1);
                }

            pthread_exit(NULL);

            sem_destroy(&der);
            sem_destroy(&horiz);
            sem_destroy(&izq);
            sem_destroy(&enter);
            sem_destroy(&arriba);

}


