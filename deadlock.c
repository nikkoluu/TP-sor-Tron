#include <stdio.h>
#include <stdlib.h>    
#include <unistd.h>     // para castear de puntero a entero
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>

sem_t tenedor1;
sem_t tenedor2;
sem_t tenedor3;
sem_t tenedor4;
sem_t tenedor5;

pthread_t F1;
pthread_t F2;
pthread_t F3;
pthread_t F4;

pthread_t F5;

void esperar () {
  //simular un tiempo de ejecucion de algun script araña (algun codigo en python)

  //inicializar en 1 segundo = 1000000 microseconds:
  int microseconds = 1000000;

  //dormir el thread, simula que esta haciendo alguna tarea
  usleep(microseconds);
}

void* comer1(){
	sem_wait(&tenedor1);
	esperar();
	sem_wait(&tenedor5);
	printf("FILOSOFO 1 COMIENDO...\n");
	esperar();
	sem_post(&tenedor1);
	sem_post(&tenedor5);

            pthread_exit(NULL);
}

void* comer2(){

	sem_wait(&tenedor2);
	esperar();
	sem_wait(&tenedor1);
	printf("FILOSOFO 2 COMIENDO...\n");
	esperar();
	sem_post(&tenedor2);
	sem_post(&tenedor1);

            pthread_exit(NULL);
}

void* comer3(){

	sem_wait(&tenedor3);
	esperar();
	sem_wait(&tenedor2);
	printf("FILOSOFO 3 COMIENDO...\n");
	esperar();
	sem_post(&tenedor3);
	sem_post(&tenedor2);

            pthread_exit(NULL);
}

void* comer4(){
	sem_wait(&tenedor4);
	esperar();
	sem_wait(&tenedor3);
	printf("FILOSOFO 4 COMIENDO...\n");
	esperar();
	sem_post(&tenedor4);
	sem_post(&tenedor3);


            pthread_exit(NULL);
}

void* comer5(){
	sem_wait(&tenedor5);
	esperar();
	sem_wait(&tenedor4);
	printf("FILOSOFO 5 COMIENDO...\n");
	esperar();
	sem_post(&tenedor5);
	sem_post(&tenedor4);

            pthread_exit(NULL);

}
int main()
{
		sem_init(&tenedor1,0,1);
		sem_init(&tenedor2,0,1);
		sem_init(&tenedor3,0,1);
		sem_init(&tenedor4,0,1);
		sem_init(&tenedor5,0,1);

		int th;
		th=pthread_create(&F1,NULL,comer1,NULL);
		th=pthread_create(&F2,NULL,comer2,NULL);
		th=pthread_create(&F3,NULL,comer3,NULL);
		th=pthread_create(&F4,NULL,comer4,NULL);
		th=pthread_create(&F5,NULL,comer5,NULL);

		pthread_join(F1,NULL);
		pthread_join(F2,NULL);
		pthread_join(F3,NULL);
		pthread_join(F4,NULL);
		pthread_join(F5,NULL);
		

            pthread_exit(NULL);
            sem_destroy(&tenedor1);
            sem_destroy(&tenedor2);
            sem_destroy(&tenedor3);
            sem_destroy(&tenedor4);
            sem_destroy(&tenedor5);

	return 0;
}