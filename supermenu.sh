#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
    red=`tput setaf 1`;
    green=`tput setaf 2`;
    blue=`tput setaf 4`;
    bg_blue=`tput setab 4`;
    reset=`tput sgr0`;
    bold=`tput setaf bold`;
#------------------------------------------------------
# VARIABLES GLOBALES
#------------------------------------------------------
proyectoActual="/home/sor1";
proyectos="/home/sor1/Documents/repo_GitLab/repos.txt";

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver contenido de carpeta";
    echo -e "\t\t\t b.  Ver informacion del sistema";
    echo -e "\t\t\t c.  Buscar programa instalado";
    echo -e "\t\t\t d.  Crear usuario";        
    echo -e "\t\t\t e.  Guardar registros de usuario nuevo";        
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
    echo $1;
    while true; do
        echo "desea ejecutar? (s/n)";
            read respuesta;
            case $respuesta in
                [Nn]* ) break;;
                   [Ss]* ) eval $1
                break;;
                * ) echo "Por favor tipear S/s ó N/n.";;
            esac
    done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
        imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
        decidir "ls -l";
}

b_funcion () {
           imprimir_encabezado "\tOpción b.  Ver informacion del sistema";
    	decidir "echo "";echo "CPU:" ;  sed -n '5p' /proc/cpuinfo;echo ""; echo "KERNEL:"; uname -sr ;echo""; echo "ARQUITECTURA:"; uname -i; echo"" ; echo "GPU:"; sudo lshw -C display | sed -n '3p'; echo "" ; echo "";  ";
	#completar
}

c_funcion () {
          imprimir_encabezado "\tOpción c. Buscar un programa instalado";
          echo -e "Ingrese el programa a buscar:"
	  read program;
	 decidir "echo "PROGRAMAS.ENCONTRADOS:";dpkg -l | grep "$program" ;";
}

d_funcion () {
    imprimir_encabezado "\tOpción d. Crear usuario";
	echo "Ingrese el nombre del usuario a crear";
	read user;
	decidir "sudo useradd "$user"; sudo passwd "$user"; su -l "$user" ";
}


e_funcion () {
    	imprimir_encabezado "\tOpción e. Guardar registros de usuario nuevo";        
    	echo "Ingrese el nombre del usuario creado anteriormente";
	read user;
	decidir "grep "$user" /var/log/auth.log >> registro.txt ";
}


#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
 

